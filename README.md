# Android GridLayout

This repository is originally forked from `https://github.com/eddydn/AndroidGridLayout`  I've just updated and changes little things .

Application Screenshot: 

![layout_screenshot](app/src/main/res/drawable/GridLayout.png)

## Changelogs :
* Updated **compileSdkVersion** in 30
* Added **buildToolsVersion** 30.0.1
* Updated **implementation 'com.android.support:appcompat-v7:26.1.0'**
* Updated **implementation 'com.android.support:cardview-v7:26.1.0'**
* Updated **implementation 'com.android.support.constraint:constraint-layout:1.0.2'**
* Updated **testImplementation 'junit:junit:4.13'**
* Removed **androidTestImplementation 'com.android.support.test:runner:1.1.1'**
* Removed **androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.2.0'**
* Updated **testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"**
* Fixed Layout 

Source : 
1. [Youtube Tutorial](https://www.youtube.com/watch?v=VUPM387qyrw)
2. Original Source Code - [Github](https://github.com/eddydn/AndroidGridLayout) 

**Farhan Sadik**

*Square Development Group*